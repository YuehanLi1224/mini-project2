use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Default)]
struct ResData {
    method: String,
    result: i32,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let x1 : i32 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("x1"))
        .unwrap().parse().unwrap();

    let x2 : i32 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("x2"))
        .unwrap().parse().unwrap();

    let sum = x1 + x2;
    let mut res_data: ResData = Default::default();
    res_data.method = event.method().as_str().to_string();
    res_data.result = sum;
    let resp = Response::builder()
    .status(200)
    .header("content-type", "text/html")
    .body(serde_json::to_string(&res_data).unwrap().into())
    .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
